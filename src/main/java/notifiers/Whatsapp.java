package notifiers;

import discovery.Notifier;

public class Whatsapp implements Notifier {
	
	private String notifierName = "Whatsapp";

    @Override
    public String notify(String message) {
        String msj = "Notificación enviada por Whatsapp: " + message;
        return msj;
    }
    
	@Override
	public String getName() {
		return notifierName;
	}

}

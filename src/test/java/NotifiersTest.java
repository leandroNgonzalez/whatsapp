import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import discovery.Notifier;
import notifiers.*;

public class NotifiersTest {

    @Test
    public void notifiersWhatsapp(){
        Notifier notificator = new Whatsapp();
        Assertions.assertEquals("Notificación enviada por Whatsapp: ",notificator.notify(""));
    }
}
